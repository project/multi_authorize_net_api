<?php

/**
 * @file
 * The configuration page and form for multi_authorizenet.api.module .
 */

/**
 * Implements hook-form().
 *
 * A drupal admin form to administrate this module configurations.
 */
function multi_authorizenet_api_admin_form($form, &$form_state) {
  $form = array();
  $form['#tree'] = TRUE;

  $content_types = node_type_get_types();
  $vocabularies = taxonomy_vocabulary_get_names();
  $commerce_authorizenet_module_exists = variable_get('commerce_authorizenet_module_exists', 0);
  $instance = array();
  $enabled = array();
  $form['authorizenet_api_server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Multi Authorize.Net API Sever Settings.'),
    '#description' => t('These settings are to check the existence of an Authorize.Net account. <b>Does not affect the Authorize.Net AIM payment method for card not present transactions.</b>'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['authorizenet_api_server']['server_selection'] = array(
    '#type' => 'radios',
    '#title' => t('Select Authorize.Net Account Mode'),
    '#default_value' => variable_get('multi_auth_api_server_select', 'production'),
    '#options' => array('production' => t('Live account'), 'developer' => t('Developer test account')),
    '#description' => t('Only specify a <b>Developer test account</b> if you login to your account through https://test.authorize.net.<br>Adjust to <b>Live account</b> when you are ready to start adding Live Authorize.Net account.'),
  );
  if ($commerce_authorizenet_module_exists) {
    $commerce_product_types = commerce_product_type_get_name();

    $form['comm_product_type_list'] = array(
      '#type' => 'fieldset',
      '#title' => t('Commerce Product Type Settings.'),
      '#description' => t('Configuration for Commerce Product types to add Field that can associate an Authorize.Net API'),
      '#collapsible' => TRUE,
    );
    $description = array();
    foreach ($commerce_product_types as $type => $type_info) {
      foreach (multi_authorizenet_api_module_fields() as $field_info) {
        if (empty($description[$field_info['name']])) {
          $form['comm_product_type_list']['description'][$field_info['name']] = array(
            '#type' => 'item',
            '#markup' => t('Check to Add <b>@field</b> Field to all the Commerce Products in specified Product type.', array('@field' => $field_info['title'])),
          );
        }
        $instance[$type][$field_info['name']] = field_info_instance('commerce_product', $field_info['name'], $type);
        $enabled[$type][$field_info['name']] = (!empty($instance[$type][$field_info['name']]));

        $form['comm_product_type_list'][$type][$field_info['name']] = array(
          '#type' => 'checkbox',
          '#title' => t('@product_type (@product_type_name)', array('@product_type' => $type_info, '@product_type_name' => $type)),
          '#description' => t('Check to Add <b>@field</b> Field to all the products in
            <b>@product_type type</b>.',
            array('@field' => $field_info['title'], '@product_type' => $type_info)),
          '#default_value' => $enabled[$type][$field_info['name']],
        );
      }
    }
  }

  $form['content_type_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Type Settings.'),
    '#description' => t('Configuration for Content types to add Field that can associate an Authorize.Net API </br>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $description = array();
  foreach ($content_types as $type => $type_info) {
    foreach (multi_authorizenet_api_module_fields() as $field_info) {
      if (empty($description[$field_info['name']])) {
        $form['content_type_list']['description'][$field_info['name']] = array(
          '#type' => 'item',
          '#markup' => t('Check to Add <b>@field</b> Field to all the nodes in specified content type.', array('@field' => $field_info['title'])),
        );
      }
      $instance[$type][$field_info['name']] = field_info_instance('node', $field_info['name'], $type);
      $enabled[$type][$field_info['name']] = (!empty($instance[$type][$field_info['name']]));

      $form['content_type_list'][$type][$field_info['name']] = array(
        '#type' => 'checkbox',
        '#title' => t('@content_type (@content_type_name)', array('@content_type' => $type_info->name, '@content_type_name' => $type_info->type)),
        '#default_value' => $enabled[$type][$field_info['name']],
      );
    }
  }

  $form['vocabulary_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vocabulary Settings.'),
    '#description' => t('Configuration for vocabularies to add Field that can associate an Authorize.Net API'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $description = array();
  foreach ($vocabularies as $vocabulary => $vocabulary_info) {
    foreach (multi_authorizenet_api_module_fields() as $field_info) {
      if (empty($description[$field_info['name']])) {
        $form['vocabulary_list']['description'][$field_info['name']] = array(
          '#type' => 'item',
          '#markup' => t('Check to Add <b>@field</b> Field to all the terms in specified vocabulary.', array('@field' => $field_info['title'])),
        );
      }
      $instance[$vocabulary][$field_info['name']] = field_info_instance('taxonomy_term', $field_info['name'], $vocabulary);
      $enabled[$vocabulary][$field_info['name']] = (!empty($instance[$vocabulary][$field_info['name']]));

      $form['vocabulary_list'][$vocabulary][$field_info['name']] = array(
        '#type' => 'checkbox',
        '#title' => t('@vocabulary (@vocabulary_name)', array('@vocabulary' => $vocabulary_info->name, '@vocabulary_name' => $vocabulary_info->machine_name)),
        '#default_value' => $enabled[$vocabulary][$field_info['name']],
      );
    }
  }
  $form['auth_api_cart_error'] = array(
    '#type' => 'fieldset',
    '#title' => t('Multi Authorize.Net API Cart error message Settings.'),
    '#description' => t('This message will be displayed as error message when a user
      adds multiple products of multitple merchants into the cart.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Conditions for Setting up Cart Restriction.
  $uc_multi_authorizenet_api_module = module_exists('uc_multi_authorizenet_api');
  $comm_multi_authorizenet_api_module = module_exists('comm_multi_authorizenet_api');
  $multi_auth_api_enable_cart_error = variable_get('multi_auth_api_enable_cart_error');
  $sub_module_name = array("module_exists" => '', "module_name" => '');
  if ($uc_multi_authorizenet_api_module || $comm_multi_authorizenet_api_module) {
    $sub_module_name["module_exists"] = $uc_multi_authorizenet_api_module . $comm_multi_authorizenet_api_module;
    $sub_module_name["module_name"] = $comm_multi_authorizenet_api_module == 1 ? 'comm_multi_authorizenet_api' : 'uc_multi_authorizenet_api';
    variable_set('multi_auth_api_enable_cart_error', 0);
    $multi_auth_api_enable_cart_error = 0;
  }

  $form['auth_api_cart_error']['enable_cart_error_msg'] = array(
    '#type' => 'checkbox',
    '#title' => $multi_auth_api_enable_cart_error ? t('Cart Restriction Feature is Enabled.') : t('Cart Restriction Feature is Disabled.'),
    '#description' => $sub_module_name["module_exists"] ? t('Cart Restriction Feature cannot be toggled. Please uninstall the "!sub_module_name" submodule and then try to toggle this feature.', array("!sub_module_name" => $sub_module_name["module_name"])) : t('This feature is handled automatically based on the availability of the sub modules. If Checked then Cart Restriction feature is Turned ON. </br>This Feature will restrict the user from adding multiple products associated with multiple Authorize.Net account in the cart. (ie. The user will be able to add products that belongs to one Authorize.Net account at a time).'),
    '#default_value' => variable_get('multi_auth_api_enable_cart_error', 1),
  );
  $error_msg = variable_get('multi_auth_api_cart_error_msg');
  $form['auth_api_cart_error']['cart_error_msg'] = array(
    '#type' => 'text_format',
    '#title' => t('Cart validation error message'),
    '#default_value' => $error_msg['value'],
    '#format' => $error_msg['format'],
  );

  $form['submit'] = array(
    '#value' => t('Save Configuration'),
    '#type' => 'submit',
  );
  return $form;
}

/**
 * Initial submit handler for multi_authorizenet_api_admin_form.
 */
function multi_authorizenet_api_admin_form_submit(&$form, &$form_state) {
  // Store API Server Selection variable.
  variable_set('multi_auth_api_server_select', $form_state['values']['authorizenet_api_server']['server_selection']);
  variable_set('multi_auth_api_cart_error_msg', $form_state['values']['auth_api_cart_error']['cart_error_msg']);
  variable_set('multi_auth_api_enable_cart_error', $form_state['values']['auth_api_cart_error']['enable_cart_error_msg']);
  $content_types = node_type_get_types();
  $vocabularies = taxonomy_vocabulary_get_names();
  $commerce_authorizenet_module_exists = variable_get('commerce_authorizenet_module_exists', 0);

  if ($commerce_authorizenet_module_exists) {
    $commerce_product_types = commerce_product_type_get_name();

    foreach ($commerce_product_types as $type => $type_info) {
      foreach (multi_authorizenet_api_module_fields() as $field_info) {
        $instance = field_info_instance('commerce_product', $field_info['name'], $type);
        $currently_enabled = (!empty($instance));
        $newly_enabled = $form_state['values']['comm_product_type_list'][$type][$field_info['name']];

        if ($newly_enabled && !$currently_enabled) {
          multi_authorizenet_api_field_create_instance($field_info['name'], 'list_text', TRUE, 'commerce_product', $type, $field_info['title']);
          drupal_set_message(t('<b>@field</b> field is added to all products in @product_type bundle.', array('@field' => $field_info['title'], '@product_type' => $type)));
        }
        elseif (!$newly_enabled && $currently_enabled) {
          field_delete_instance($instance);
          drupal_set_message(t('@field_title field is now disabled for @product_type_name.', array('@field_title' => $field_info['title'], '@product_type_name' => $type_info)));
          watchdog('multi_authorizenet_api', 'Deleted @field_title (@field_name) field from @product_type_name type.', array(
            '@field_title' => $field_info['title'],
            '@field_name' => $field_info['name'],
            '@product_type_name' => $type,
          )
          );
        }
      }
    }
  }

  foreach ($content_types as $type => $type_info) {
    foreach (multi_authorizenet_api_module_fields() as $field_info) {
      $instance = field_info_instance('node', $field_info['name'], $type);
      $currently_enabled = (!empty($instance));
      $newly_enabled = $form_state['values']['content_type_list'][$type][$field_info['name']];

      if ($newly_enabled && !$currently_enabled) {
        multi_authorizenet_api_field_create_instance($field_info['name'], 'list_text', TRUE, 'node', $type, $field_info['title']);
        drupal_set_message(t('<b>@field</b> field is added to all nodes in @content_type content type.', array('@field' => $field_info['title'], '@content_type' => $type)));
      }
      elseif (!$newly_enabled && $currently_enabled) {
        field_delete_instance($instance);
        drupal_set_message(t('@field_title field is now disabled for @content_type_name.', array('@field_title' => $field_info['title'], '@content_type_name' => $type_info->name)));
        watchdog('multi_authorizenet_api', 'Deleted @field_title (@field_name) field from @content_type type.', array(
          '@field_title' => $field_info['title'],
          '@field_name' => $field_info['name'],
          '@content_type' => $type,
        )
        );
      }
    }
  }

  foreach ($vocabularies as $vocabulary => $vocabulary_info) {
    foreach (multi_authorizenet_api_module_fields() as $field_info) {
      $instance = field_info_instance('taxonomy_term', $field_info['name'], $vocabulary);
      $currently_enabled = (!empty($instance));
      $newly_enabled = $form_state['values']['vocabulary_list'][$vocabulary][$field_info['name']];

      if ($newly_enabled && !$currently_enabled) {
        multi_authorizenet_api_field_create_instance($field_info['name'], 'list_text', TRUE, 'taxonomy_term', $vocabulary, $field_info['title']);
        drupal_set_message(t('<b>@field</b> field is added to all terms in @vocabulary vocabulary.', array('@field' => $field_info['title'], '@vocabulary' => $vocabulary)));
      }
      elseif (!$newly_enabled && $currently_enabled) {
        field_delete_instance($instance);
        drupal_set_message(t('@field_title field is now disabled for @vocabulary_name.', array('@field_title' => $field_info['title'], '@vocabulary_name' => $vocabulary_info->name)));
        watchdog('multi_authorizenet_api', 'Deleted @field_title (@field_name) field from @vocabulary_name vocabulary.', array(
          '@field_title' => $field_info['title'],
          '@field_name' => $field_info['name'],
          '@vocabulary_name' => $vocabulary_info->name,
        )
        );
      }
    }
  }
  drupal_set_message(t('Your configurations has been saved. Authorize.Net Transaction Mode is <b>@server</b> mode.', array('@server' => variable_get('multi_auth_api_server_select', 'production'))));
}
